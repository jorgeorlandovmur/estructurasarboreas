/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

/**
 *
 * @author madarme
 */
public class NodoBin<T>  {
    
    private T info;
    //Para los subárboles izquierdos
    private NodoBin<T> izq;
    //Para los subárboles derechos
    private NodoBin<T> der;

    public NodoBin() {
    }

    public NodoBin(T info, NodoBin<T> izq, NodoBin<T> der) {
        this.info = info;
        this.izq = izq;
        this.der = der;
    }

    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public NodoBin<T> getIzq() {
        return izq;
    }

    public void setIzq(NodoBin<T> izq) {
        this.izq = izq;
    }

    public NodoBin<T> getDer() {
        return der;
    }

    public void setDer(NodoBin<T> der) {
        this.der = der;
    }
    
    
    
    
}
