/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

import java.util.Iterator;

/**
 *
 * @author madar
 */
public class ArbolBinario<T> {

    private NodoBin<T> raiz;

    public ArbolBinario() {
    }

    public NodoBin<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }

    public int getContarHojas() {

        return this.getContarHojas(this.raiz);
    }

    private int getContarHojas(NodoBin<T> r) {
        if (r == null) {
            return 0;
        }
        int c = this.esHoja(r) ? 1 : 0;
        return c + this.getContarHojas(r.getIzq()) + this.getContarHojas(r.getDer());
    }

    private boolean esHoja(NodoBin<T> x) {
        return (x != null && x.getIzq() == null && x.getDer() == null);
    }

    /**
     *
     * @return un entero con la cantidad de hojas que están a la derecha
     */
    public int getCanHojasDerechas() {
//        return getCanHojasDerechas(this.raiz);
        return getCanHojasDerechas(this.raiz, false);

    }

    private int getCanHojasDerechas(NodoBin<T> r) {
        if (r == null) {
            return 0;
        }
        int c = this.esHoja(r.getDer()) ? 1 : 0;
        return c + this.getCanHojasDerechas(r.getDer()) + this.getCanHojasDerechas(r.getIzq());
    }

    public int getCanHojasDerechas(NodoBin<T> r, boolean derecho) {
        if (r == null) {
            return 0;
        }
        int c = this.esHoja(r) && derecho ? 1 : 0;
        return c + this.getCanHojasDerechas(r.getIzq(), false) + this.getCanHojasDerechas(r.getDer(), true);

    }

    public String getLukasiewicz() {
        if (this.esVacio()) {
            throw new RuntimeException("Árbol vacío");
        }
        return this.getLukasiewicz(raiz);
    }

    private String getLukasiewicz(NodoBin<T> r) {

        if (r == null) {
            return "b";
        }
        return "a" + this.getLukasiewicz(r.getIzq()) + this.getLukasiewicz(r.getDer());

    }

    public boolean esVacio() {
        return this.raiz == null;
    }

    /**
     * Recorridos: Linealizar el àrbol
     */
    public Iterator<T> preOrden() {
        ListaCD<T> l = new ListaCD();
        preOrden(l, this.raiz);
        return l.iterator();
    }

    private void preOrden(ListaCD<T> l, NodoBin<T> r) {
        if (r == null) {
            return;
        }
        l.insertarFin(r.getInfo());
        preOrden(l, r.getIzq());
        preOrden(l, r.getDer());
    }

    public Iterator<T> inOrden() {
        ListaCD<T> l = new ListaCD();
        inOrden(l, this.raiz);
        return l.iterator();
    }

    private void inOrden(ListaCD<T> l, NodoBin<T> r) {
        if (r == null) {
            return;
        }

        inOrden(l, r.getIzq());
        l.insertarFin(r.getInfo());
        inOrden(l, r.getDer());
    }

    public Iterator<T> posOrden() {
        ListaCD<T> l = new ListaCD();
        posOrden(l, this.raiz);
        return l.iterator();
    }

    private void posOrden(ListaCD<T> l, NodoBin<T> r) {
        if (r == null) {
            return;
        }

        posOrden(l, r.getIzq());
        posOrden(l, r.getDer());
        l.insertarFin(r.getInfo());
    }

    public int getCardinalidad() {
        int x[] = {0};
        this.getCardinalidad(this.raiz, x);
        return x[0];
    }

    /**
     * versión 1
     *
     * @param r nodo binario que referencia cada raìz de un subàrbol
     * @param x un objeto wrapper , en este caso es un vector de entero
     */
    public void getCardinalidad(NodoBin<T> r, int[] x) {
        if (r == null) {
            return;
        }
        x[0]++;
        //System.out.println("En recursión:"+x);
        this.getCardinalidad(r.getIzq(), x);
        this.getCardinalidad(r.getDer(), x);
    }

    /**
     * Versión 2: public int cardin(NodoBin<T> r) { if(r==null) return 0; return
     * 1+cardin(r.getIzq())+cardin(r.getDer()); }
     */
    public String getElementos_Niveles() {
        String msg = "";
        if (!this.esVacio()) {
            Cola<NodoBin<T>> direcciones = new Cola();
            direcciones.enColar(this.raiz);
            while (!direcciones.esVacia()) {
                NodoBin<T> r = direcciones.deColar();
                msg += r.getInfo().toString() + "\t";
                if (r.getIzq() != null) {
                    direcciones.enColar(r.getIzq());
                }
                if (r.getDer() != null) {
                    direcciones.enColar(r.getDer());
                }
            }

        }

        return msg;
    }

}
